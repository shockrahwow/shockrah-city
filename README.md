[![pipeline status](https://gitlab.com/shockrah/shockrah-city/badges/master/pipeline.svg)](https://gitlab.com/shockrah/shockrah-city/-/commits/master)

# Hello

Souce for all things related to [my personal website](https://shockrah.xyz)

Built with Hugo and a custom theme, more information on this theme can be found
here: [shockrah.xyz/posts/hugo](https://shockrah.xyz/posts/hugo/).

# Contact

Email: `dev@shockrah.xyz`

Mastodon: https://qoto.org/@shockrah

Discord: shockrah#2647
