---
title: "DWM and POP!\_OS"
date: 0000-00-00
draft: true
---

# DWM and POP!\_OS


## tl;dr

Make a `/usr/share/xsessions/dwm.desktop` file with contents listed below and a /comfy/ script.

## Doing the thing tho

1. We need a `/usr/share/xsession/dwm.desktop` file. Mine looks like this:


```
[Desktop Entry]
Name=DWM
Comment=This session logs in to DWM
Exec=/home/shockrah/Rice/dwm-launcher.sh
Type=Application
```

Nothing to crazy its basically a `toml` config with an executable as the entry point of the DE.
For reference here's the official Pop OS  config:

```
[Desktop Entry]
Name=Pop
Comment=This session logs you into Pop
Exec=env GNOME_SHELL_SESSION_MODE=pop /usr/bin/gnome-session --systemd --session=pop
TryExec=/usr/bin/gnome-shell
Type=Application
DesktopNames=pop:GNOME
X-GDM-SessionRegisters=true
X-Ubuntu-Gettext-Domain=gnome-session-3.0
```

This time there's more fluff but you can ignore it for the most part(I think).

2. Now let's look at that script I have have [/home/shockrah/Rice/dwm-launcher.sh]:

```sh
#!/bin/sh
# This script is used to mimic an xinitrc to some degree

set -x
eval `ssh-agent`

compton --backend glx --xrender-sync --xrender-sync-fence -b
bright 50 DWM# custom brightness script

feh --randomize --bg-fill ~/Pictures/Papes/* &

statline &
exec dwm
```

Nothing crazy right? Yeap that's it really to setup DWM with any changes you might have easily with Pop OS.

In case you're wondering here's what my setup currently looks like:

![Background Images I found on /wg/](/media/img/desktop/dwm-pop.png)
