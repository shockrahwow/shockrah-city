---
title: Esports Post
date: 0000-00-00
draft: true
---

# Esports Post

So for the past week or so I've been deliberating progress over an e-sports related project of mine: [scuffed-league](https://gitlab.com/shockrah/scuffed-league).
The whole idea here is that I like tournaments I still believe that proper long-running leagues are the preferable choice for keeping spectators and competitors around for long periods of time.

## The plan for building

Let's go through the process of building from start to current.

1. I wanted to replicate the experience of using sites like cevo or esea from my own competitive history but, _those have had years to build and mature_.

Something which to be honest never really struck until I had built a few bare pages. I say bare because quite frankly, every page I made had nearly no content to show for. It was also unlikely that I was going to be able to fill them with content for a long time.

Issue: cevo/esea/faceit all support multiple leagues, with multiple divisions in multiple games with multiple tournaments and prizes, news, drama, editors, etc.
There was no way I could fill all these things like a full team of content creators could.

Solution: Start with one game, _Diabotical_ with one league with one division, and a second game mode.

This meant that I could make a ~~meme~~one-page webapp thing.
As the name implies its literally one page which makes it really easy to fill the page with content.
At my scale the amount of content I have to serve up is so small that things shouldn't feel cluttered but will still feel like there's a lot going on.


2. I have some data to store and some of it is really strange do I go with SQL, Redis, Firebase, what?

Some data fits really easily into a table: like user data. Every user can be part of 1 team-league 1 duel-league and then has some basic info about them username/password.
Jam that into a table and its easy to query, update, and remove users.

Even the team data isn't too bad: `captain|member2|member3|teamname|?`. The only issue is the match data.
The problem is how we store match history.

3. I have to store match history for each team and it has to relate to other teams as entries could be related to one another.

If that prompt makes it seem like each team requires a structure to hold previous matches then you would be right.
If I had gone with this question to answer it I would have ended up with a really weird system where I would have to cross reference things in multiple databases and then worry about huge latency.

Reframing the question to "_What data does 1 match really have_".
To actually get to this point however I had to go through a few iterations of holding match data.

![Scratch work showing my progress](/media/images/scuffed_matches_progress.png)

