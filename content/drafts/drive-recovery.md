---
title: "Drive Recover"
date: 0000-00-00
draft: true
---

# Drive Recovery

I've gone through too many hard drives and solid state drives to count and they've piled up over the years. So let's find out which ones are actually dead, and which ones I can recover.

For each of these tests I use a combination of the following tools to diagnose what's wrong with the drives.
Not the data or the file systems but the physical drives because if the hardware is good then I should be able to install whatever file system I want and move on.
_Everything here is done on Debian as the tooling is super easy to get a hold of_.

```
badblocks
	Check for bad blocks on the drives
smartctl
	Grab SMART data off the drives
mount
	If a filesystem is found I can pull stuff off the drive once mounted
	Most drives don't have anything though
```

## Kingston SSD

First mounting drive to see what its about `mount` tells me the filesystem is unclean
It still mounted but all I found was this:

![](/media/img/drives/dir.png)

It should be noted that this drive has gone through multiple partition schemes.
Its also gone from [Windows] to [Grub, Windows, Linux] back to [Windows, Linux] and then back again at some point.

With `sudo badblocks -v /dev/sdc1 > bads.txt` no errors were found so I moved on from there.

Finally I used `smartctl` which can be found in the Debian buster repositories in the `smartmontools` package.
Luckily my drive was found in the tool's database so it was able to read back useful data from the drive.

One `smartctl -A /dev/sdc` later and I found out that the drive was basically functioning normally, nothing was out of the ordinary.

If you have a Kingston SSD and don't know what the data means then [look at this](https://media.kingston.com/support/downloads/MKP_306_SMART_attribute.pdf).

At this point I just reformatted the drive and moved on to the next drive: a Western Digital Blue 1TB Drive.

## Western Digital Blue 1TB

This drive is somewhat new as my old WD Blue was giving me issues when booting into Windows, and for some reason contained another old Grub installation
