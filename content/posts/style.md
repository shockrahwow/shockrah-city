---
title: Wot in tarnation happened to the style
date: 2017-09-21
draft: false
category: article
description: Updating the style
---
# Wew lad new colors

It's been about a minute since I've done anything with this site but I figured what better time to randomly post things then now.
Before this update I mostly used some neato fonts but most of it was some boring Times New Roman thing, _now the site doesn'tlook like a homework essay_.

## But Why?

I partly wanted to get my Gitlab page a bit more active but also because I didn't like the self-conflicting style from before.
Instead of having a bunch of fonts that don't really go together I opted for fonts that are actually somewhat look nice next to each other.

I'm still gonna mess with the styling but the changes likely won't be whole reworks like this one was, however, I'll probably use this as a place to post some _bhop stuff as well_.

:^)

