---
title: "Website Contruction Philosophy"
date: 2018-06-01
draft: false
category: article
description: Why the website is built this way
---
# No Non-free JS here

To explain succintly, I don't like Javascript, especially considering how much power it has on a user's browser.
Namely the issue being that while it used to mainly be focused around making websites look pretty it now also serves to execute malicious code; luckily I don't think that is too common but that fact alone makes JS for me, something which should be avoided. 

It's JS's non-free abilities which I don't like and is why I am not going to be using it for any of my projects, unless it is a client-side only project where it is running on some local server. 

There are of course a few *tangent* reasons why I decided to fully make the switch from using the Hugo static site generator:

* Performace

Loading a bunch of JS/php is rather cumbersome at time which can often be tracked down to the js being obfuscatedpoorly, creating a file which is sometimes hundreds of times larger than normal. 
I'll show by example - namely using *r-markdown* and then my own technique to make a page with identical content:

```
# This is an example header

With a small paragraph

* a 

* List

> and a quote
```

* Dev time/ Convenience

To be completely honest I'm reall not interested by web-development much at all, so finding a flow to make creating content easier is just perfect for me. *Also I dont have to deal with running wine/windows or dealing with hundreds of dependencies with __Hugo__.*

Hopefully this explains some of my thought process behind this site's design, and if you like, check out some of my other stuff that I post here. 

