---
title: Nvidia Drivers on Debian Sid
date: 2021-12-27
draft: false
description: Finally a  working config
category: article
---

# Packages you need

To detect the right type of driver to install `apt` repositories have a package
called `nvidia-detect` which I recommend running first. In my case running a 
_GTX 2060_ I had to install the generic `nvidia-driver` package

**Before you install the driver package!**

Grab the apt package `nvidia-xconfig`. If you're like me and never bothered to
an `/etc/X11/xorg.conf` this will write out a config for you that should,
_just werk_.

# Installing

Commands are listed in order, just be sure to run with correct permissions.

```
apt install nvidia-detect
package=$(nvidia-detect | grep '^ *nvidia')
apt install "$package"
nvidia-xconfig
```

Note: Pretty much all of the above commands, except for line 2 require elevated
permissions.
