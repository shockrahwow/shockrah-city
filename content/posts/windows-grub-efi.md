---
title: Fixing a windows EFI boot directory
description: Because I can't into Grub
date: 2023-10-27T18:27:05-07:00
image: /favicon.png
draft: false
category: article
---

# Context

After making my usual Debian drive unusable ( thanks Nvidia ) from an 
`apt upgrade` I found myself re-installing Debian all over again. Luckily
I keep my `/home` and `/` mounted on separate partitions so this was
pretty easy. When it came time to pick a drive to boot off of however I comletely
forgot that I had a Windows drive that I had been using to jankily boot off of.

_Sidenote_: I only forgot because I never bothered to remedy the situation of
being forced to smack F12 9999 times every time I booted for a few months. :shrug:

After re-installing Debian and getting setup once more I went to boot into
Windows since I had some avatar modeling I wanted to do and.... couldn't boot.

# Discovering what happened

From Debian, check where the `/boot` is mounted to.
In my case I have drives `sda` and `sdb` which contain Debian and Windows data
respectively. I found the following

1. `/boot` was mounted to `sdb`
2. `/boot/efi/EFI/` did not contain any sort of Windows folder

:mag: **HINT:** there are tools in Debian repos to repair this folder however
I tested literally none so good luck.

Basically the new Grub installation had wiped out the old `/boot` partition
which I never paid attention to and removed it :facepalm: hence why it had
no Windows section. Normally an `update-grub` would suffice **if** that folder
was present but this was not enough.

# Solution

To make Windows bootable I setup a Windows installation USB drive using dd as such:

```bash

dd if=/path/to/Windows.iso of=/dev/<usb-drive> status=progress
```

Also this will hang when it gets to the end but seriously just give it some time
because it basically has to verify everything after it's been copied over.

Finally I found this post on superuser which shows how to bring back the EFI
bootloader for windows so that Grub create a new entry for it and we can boot
like normal :arrow_right: [answer](https://superuser.com/a/1444266)

# Raw paste of Solution from 🌐 [superuser](https://superuser.com/a/1444266)
In case that answer ever gets clobbered here is the raw paste:


The other answers given here work great on MBR/BIOS systems, however if you're
on a UEFI system like I am, bootsect will just write a semi-functional boot MBR
over the [🌐 GPT protective MBR and bootrec](https://en.wikipedia.org/wiki/GUID_Partition_Table#Protective_MBR_(LBA_0))
just gives an "Access denied" error message, and neither one has a functional
option to fix a broken [🌐 EFI system partition](https://en.wikipedia.org/wiki/EFI_system_partition),
which on a UEFI/GPT drive is what
contains the bootloader that used to be stored in the MBR. There's unfortunately
almost no up-to-date guides on fixing the UEFI Windows Boot Manager (almost all
of them just say to run the graphical Startup Repair utility, but that doesn't
fix the problem in all cases), but I finally found the correct solution buried
in [🌐 this article](https://www.partitionwizard.com/clone-disk/bootrec-fixboot-access-is-denied.html),
which requires the use of the [🌐 bcdboot](https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/bcdboot-command-line-options-techref-di) command instead:

1. Grab the [🌐 Media Creation Tool](https://www.microsoft.com/en-us/software-download/windows10), make yourself a Windows 10 installation DVD or USB drive, and then boot into it.

2. When prompted, choose "Repair your computer", followed by "Troubleshoot", "Advanced Options", and finally "Command Prompt".

3. Run `diskpart` and then `list disk`. Note the disk number for the disk with your EFI system partition (ESP).

4. Select that disk with `select disk x` (where `x` is the disk number from the last step).

5. Run `list volume`. Note the volume number for your EFI system partition (ESP).

6. Now do `select volume x` (where `x` is the volume number for the ESP) and then
`assign letter=N:` to mount the partition. Run `list volume` again and note that
the ESP is now assigned a driver letter. Run `exit` to leave `diskpart`.

7. (Optional) If you are not currently dual booting and want to fully clean the
ESP before writing a new bootloader, run `format N: /FS:FAT32` to reformat it as
FAT32. This is probably not necessary under normal circumstances, however, as
`bcdboot` seems to do a good job of cleaning things up itself. Especially **do not
do this if you have a Linux distro on another partition** or else you'll have to
reinstall GRUB as well once you're done with this. Also note that the following
steps should not affect an EFI GRUB install as long as you do not otherwise
delete GRUB's existing directory on the ESP.

8. Finally, write the new bootloader to the partition with `bcdboot C:\windows /s N: /f UEFI`.
This command rebuilds a new UEFI-compatible bootloader on the ESP mounted at `N:`
using the Windows installation mounted at C:\windows. Once it's done, you can
verify the new bootloader was written by running `dir N:\EFI`, where you should
see a `Microsoft directory` containing the new Windows Boot Manager as well as a
`boot directory` containing the fallback bootloader (along with other directories
for any other bootloaders you have installed, such as GRUB for Linux).

9. (Optional) If you are dual booting, you will probably need to boot into your
Linux distro and run `sudo update-grub` to allow the GRUB scripts to detect and
add the new Windows bootloader.

10. Now boot into your BIOS setup and make sure "Windows Boot Manager" (or GRUB,
if you're dual-booting) is set as the top boot choice. Save and reboot and
you'll finally be back in Windows (or GRUB).


Here are some links to help you understand EFI stuff ( because I still don't )

* https://askubuntu.com/questions/1144636/three-questions-on-boot-efi-and-boot-mountpoints
* https://support.microsoft.com/en-us/topic/use-bootrec-exe-in-the-windows-re-to-troubleshoot-startup-issues-902ebb04-daa3-4f90-579f-0fbf51f7dd5d
