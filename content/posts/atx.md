---
title: "Carry On PC "
date: 2018-01-10
draft: false
description: How I crammed a full ATX board into a mATX Case
category: article
---

# Carry On PC

I use my PC for... well pretty much everything.
Which means if I go somewhere for a few weeks at a time, my PC is coming with me.
For a while I used a _Corsair 300r_, which has served me well until now.
After looking around for a travel friendly ATX case I realized that there really aren't any.
For that reason I would have to find a case that my parts physically fit into, and cram them in somehow.

## Prepping the case

To start the case I bought was a [Cooler MasterN200](http://us.coolermaster.com/product/Detail/case/n-series/n200.html).
The main parts I had to worry about were as folow in the table: 


| Part | Length/Size | Name |
|---|---|---|
| MotherBoard | ATX | Gigabyte Z97X-UD4H |
| CPU Cooler | 6.5in tall | Hyper 212 Evo |
| Graphics Card | 10 in long | EVGA 960 2Gb SC Edition |

After opening the case I saw a few things that had to go.

{{< pic "/media/img/atx/before.jpg" >}}

1. Hard Drive bays

{{< pic "/media/img/atx/hdd.jpg" >}}

I also took off the front IO panel since I have some plans to make this more flush later on, thus saving space to fit into a carry on bag.

2. PSU rack holding thingy

I have no idea what this thing is but it had to go since it wouldn't let my motherboard fit into the case.
Luckily there was only a few rivets holding it in to a quick chiseling and they were gone with the rack too.

{{< pic "/media/img/atx/rack.jpg" >}}

## Test fitting some things

First I checked how the motherboard would fit into the case on its own; I was afraid of possibly adjusted the rear I/O sheild hole.

{{< pic "media/img/atx/mbtest.jpg" >}}

Next I fiddled with the psu since fitting it into the case was going to be _really_ akward.
It ended up being as weird as I though but ultimately it works and seems to be stable so what you see in the picture is the setup I went with.

{{< pic "/media/img/atx/psutest.jpg" >}}

I completely forgot about the hard drives until the end, but it worked out in a _totally not ghetto way_.
The 3.5 inc drive is the only one that I _really_ need to worry about for mounting honestly because of two reaons:

* 1 of the remaining drives is an _SSD_
* The other drive is a laptop drive \
	Which I know still doesn't excuse the mounting but hey it coulnd't really move even if it wanted to.

{{< pic "/media/img/atx/hddfinal.jpg" >}}

Hard drives have a little shroud which I made out of some heavy duty duck tape and cardboard.
Kinda sketchy but not really since it holds things in a clean position and doesnt rattle at all.

There's one more ssd that you don't see but its tucked way up in the cd drive bay up top as I dont actually use that bay at all.

All in all I like how everything turned out as this pc is now ready to be taken anywhere in the US as _carry-on_ luggage!
