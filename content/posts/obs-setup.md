---
title: "Setting up OBS "
date: 2021-03-19
draft: false
category: article
description: For people with better stuff to do
---
# Setting up OBS - for people with better stuff to do

This is going to be a very casual post so don't expect huge a
## Before anything

1. Make sure you have hardware encoding enabled; especially if you have a dedicated GPU.
You can find this setting under `Output -> Encoder`

2. Setup a hotkey for muting your microphone `Hotkeys -> Mix/Aux [Mute+Unmute]`. I personally use `Ctrl + Shift + Space`, also these two are coupled together so they are always the same(thank me later when).

3. Bit rate is used to describe how much data you can "spread" over 1 second. Terrible analogy incoming: `bit-rate = butter` and `bread size = resolution`. The more butter you have the more you can cover the bread. 

Too much bread(resolution), not enough butter(bit-rate)? Then your image quality is trashed.

Too much butter(bit-rate), not enough bread(resolution)? Great image quality but after a point it's wasted due to the resolution.

## Recording

> What bitrate/fps/resolution should I use?

5000 KBPS is pretty much the baseline in my opinion for recording as the quality will very quickly look like shit the lower you go from here(at 1920x1080 and up).

> Will it look good?

It will be passable at 1920x1080, it won't be bad but it won't be good either, it will be average enough that you probably won't notice **unless**...
The amount of _visual noise_ is really high then you'll actually get real value from cranking up the bitrate.

Personally I use 1920x180 @ 10,000 Kbps since most of the games I play regularly don't have much visual noise or I turn off those "eye clutter" setting off anyway. Games like Battlefield, Battlefront, and Titanfall are good examples of games where you want to start at 10K and work your way up/down depending on how your PC handles things.

> How can I not destroy my hard drive and get decent quality

1280x720 @ 5000 Kbps. No I'm not kidding, 1280 is a really good for non-fullscreeen content and at 5K bit-rate you're hardpressed to find games that _don't_ look good.

> How do I not instantly fill my hard drive?

Bitrate. This is the thing that affects the amount of space required on a harddrive or a network, the resolution does nothing to the file size. 
For some perspective:

* 10000 Kbps is roughly 1.25 megabytes per second


## Streaming

> How do I make my stream look really good like those pr0 streamers on Twitch 

Become a partner. In all seriousness, the maximum bit rate allowed by twitch(unless your partnered) is literally 6000 Kbps. They recommend streaming at 1920x1080 but if I'm being honest it's not worth it. Lower resolution + higher bitrate typically results in more clear image quality. The other added benefit is that lower resolutions require less bit-rate to actually look decent.

> CBR v VBR [Constant Bit-Rate v Variable Bit-Rate]

If OBS needs to(for performance) it will drop frames. Enabling VBR means that OBS will try to drop bit-rate when it needs to instead.
The rule of thumb is this(for twitch at least): any pc + stable internet => just use CBR. Bad internet? either stream at 48 fps + low res + lowish(3000Kbps) bit rate and you'll probably be fine.

## Audio

There is no setting that can match a decent physical setup so here goes:

* You can find mic booms for like 20 USD online
* A decent-ish microphone can be bought for like 15 USD online(check out Pyle or like anyone that sells entry level music/recording gear)

These two together will get you: decent quality + decent sound isolation. Also put a cloth at the base of the stand to isolate even more from taps/bumps on the desk.

> What if I don't want to buy a microphone?

For most people there are two filters that will give the most notable changes(maybe three).
_Warning: the following explanations will probably upset every audiophile/sound engineer in  999 mile radius here goes anyways_.

* Gain

This is similar to volume in that increasing it makes your overall sound louder, however, this will also make your sound more distorted.
For clean voice tracks on a stream you'll have to find a balance between loud/distorted("dirty") sound.


* Noise Suppression

As the name implies it can be used to reduce background noise(in theory).
In my experiences this actually does very little and is nowhere near as powerful through OBS as you may think.
If you want proper background noise reduction then lower the sensitivity of your mic and just jam it into your face.

## Clipping - Replay Buffer

This will use the same settings as the recording setup although I suggest setting up a key to save the current buffer.

Be wary that even though you saved the current buffer does not mean that the buffer you saved is flushed out.
No; in fact when you save a replay buffer saving again immediately afterwards will save the past `x` seconds even if that time overlaps with the previous buffer's time.

