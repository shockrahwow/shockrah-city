---
title: NY Egg Cream and DragonFruit Soda
date: 2021-09-19
draft: false
description: Afterthoughts on making two random drinks
category: article
image: "/media/thumbnails/dragon-soda.jpg"
---

## Preface

I'm aware of how these two have nothing to do with each other but here's my 
ramblings on how this went and what I learned making them anyway.

## Dragon Fruit Syrup - Used for the Soda

Heads up this is a _very_ mild flavored soda and thus I recommend using using the syrup more as a dye since basically anything/everything _will_ overpower its flavor.

### Ingredients

* 1/4 Cup White Sugar

* 1/4 Cup Water

* 1/4 Dragon Fruit (with the skin)

Some recipes don't call for the skin however including does give a more _tropical/wild_ taste that I personally really liked.
Being that the fruit has a very mild taste I would suggest keeping it in to give more of a chance to be felt in drinks afterwards otherwise its basically just a dye.

### Process

* Mix the water, sugar, and fruit together

* Bring the mixture up to a boil

Will look something like this while boiling, however I would also mix and smash
this a bit with a spoon while boiling: 

{{< pic "/media/img/dragonfruit/simmer.jpg" >}}

* Keep boiling for about 5-7 minutes

I should note that I was basically guessing on time here however the finished syrup was decently fragrant anyway.

## Making the soda - (The easy part)

Mix in the newly made syrup, ice and club soda and you're done.
I've added some Dragon fruit chunks to the mix for pizzazz however they ended up
helping the flavor a ton in my opinion.

{{< pic "/media/img/dragonfruit/chunks.jpg" >}}

Basically the flavor is _extremely_ mild however the texture of the fruit itself
is similar to a kiwi. Even after **loading** the glass with more syrup there still
isn't much flavor and the bite of it all really overwhelms it. While I'm not surprised
I should mention that this would work really well as a dye since the color it gives off is a really pretty pinkish/purple (which also stains just about everything it touches).

{{< pic "/media/img/dragonfruit/soda.jpg" >}}

meh/10 but at least it was refreshing.

## NY Egg Cream

### Ingredients

* 1-2 ounces Chocolate syrup

* 1-2 ounces of Milk

Any kind of milk should be fine. Personally, I used a 2% lactose-free milk.

* Disproportionate amounts of club soda

This one depends on how big the glass is + taste.

### Process

* Pour in the chocolate syrup, making sure to get some on the sides of the glass

The pouring on the sides actually help with the mixing later(source: dude trust me).


{{< pic "/media/img/nyeggcream/choco-sides.jpg" >}}

* Pour the milk in next

* Pour up to a third of the way up with club soda, the rest will just be head

* Mix **vigorously**

My suggestion for mixing is to combine a swirly motion and a stabby motion.
More like a tilted swirly motion.
The mixing should be fast and not at all gentle.
The idea here is that we _want bubbles_ to make a nice foamy head to sip through.
Remember: the foamier the better since we _want_ a really tall head of foam on 
this drink as its going to dissipate really quick.

The taste of course is like chocolate milk but its really the texture of this drink that makes it so special and unique. The head also really adds the experience so I suggest _not drinking with a straw_ to get the ⓕⓤⓛⓛ ⓔⓧⓟⓔⓡⓘⓔⓝⓒⓔ.

### Final notes

This isn't a drink you nurse since the head dissipates so quickly.
I haven't tried it but maybe with some ice cream the head would last longer so try
messing with the original recipe to get that head to stick around.


{{< pic "/media/img/nyeggcream/finished.jpg" >}}

