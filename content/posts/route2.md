---
title: Segmented Routes
date: 2019-10-02
draft: false
category: article
description: A better rendition of the previous article
---
# Segmenting & Optimizing Realistic Routes

Finally after a few weeks of practicing I took some time to segment the route that I've been working on for _bhop_bochadick_.
Something which I have a really bad habit of not doing.

Unlike a lot of other maps I've tried running in the past this one doesn't really get old and the _rng_ in the run is pretty bearable.
Apart from the route being fun there's also a few _reset points_, where if I lose time or fail it doesn't mean the run is completely over/results in +30.


> Alright but where's the route mang

Here you go :^)

<iframe width="560" height="315" src="https://www.youtube.com/embed/_Cn2m1dn0tE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


> Can it be improved?

Yes, and there's already some new tricks/optimizations I'm planning on using to bring the time down even further.

> So it's not perfect?

Nop, in fact there's a huge mistake at the top of the pyramid where I bump into a wall pretty hard, losing nearly 400 u/s(from ~900u/s to ~500u/s).

Also I could potentially do the drop in the lava stage without the wall bump thus conserving speed and cutting out a jump or two (i.e. 0.7 - 1.4 seconds).

## Stats

* Current estimated: 4:41
* Future estimated: 4:3x

