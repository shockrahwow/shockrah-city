---
title: Making Alfajores
date: 2022-12-25
draft: false
description: Peruvian Alfaores
category: article
image: /media/img/alfajores/main.jpg
---
{{< pic "/media/img/alfajores/main.jpg" >}}

# Ingredients

* Flour - _1 Cup_
	* Don't worry about packing this down when measuring
* Corn Starch  - _1 Cup_
	* Don't worry about packing this down when measuring
* Powered Sugar - _6 Tbsp._

* Unsalted butter - _200 g. ➡ 220 g._
	* Make sure the butter is soft and cut into chunks for when you mix it

# Steps

1. Sift together all the dry ingredients into a large bowl.

{{< pic "/media/img/alfajores/sifted.jpg" >}}

2. Adding butter chunks to dry in bowl.

**Mixing technique**: Use only your fingers, no palms. Basically just
grab a bunch of the mix with the fingers like a spider then pull it together
then sprinkle it back in.

Below is a clip of what the final texture should look like, just make yours
a **tiny bit more moist**.

{{<clip "/media/img/alfajores/texture.mp4" >}}

3. Mix everything until you have a nearly sandy quality about the mix.



It will feel like wet sand, that's correct. It may need some help to maintain
structure however it shouldn't just fall apart either.

4. Chill in the fridge for _15 minutes_.

5. Roll out to be about the same thickness as a pie crust (4/5 mm) and cut into small disks.

6. Finally bake for 11 minutes at 350° F.

# Words of Caution

* Bake on parchment paper to easily peel them off later

* Once they are cooked ( and before ) the caps have basically 0 structural integrity so handle with lots of care.



