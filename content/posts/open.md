---
title: "This time for real"
date: 2018-06-18
draft: false
category: article
description: Learning to build things properly
---

# All done setting up

Finally after some struggling with various git clients and learning to use some new tools the source code for this website is finally up and hosted on Gitlab.

> [Link to website source code](https://gitlab.com/shockrah/shockrah-city)\
> *tbf i stiill need to work on the bash script to auto build stuff*

## Now what?
This would have gone by more  quickly if it wasn't for the fact that I have recently decided to switch off of GitKraken since it uses a proprietary license.
The main thing I liked about it was that it made pushing, pulling, and dealing with large branches and merges very easy. 
After some testing with other git clients I ended up decided that it would be easier to just learn to use the terminal commands which git comes with.

For now this site will be maintained to journal and document things which interest me so that I can come back to it after some time and maybe use some of what I learn over time.
If it grows to be something larger; or at least something with an audience, then I may also do some stuff for the interest of the readers/audience as well, but we'll see in due time where this all goes.

The next thing for this site is to post some stuff that I never posted for the old one, namely a simple headphone mod and do some stuff with bash scripting.
Also maybe some random notes about cool stuff I find.

_Published: June 18, 2018_
