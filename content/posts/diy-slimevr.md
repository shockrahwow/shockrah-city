---
title: Building SlimeVR Trackers for Full Body Tracking
date: 2023-03-29
draft: false
description: From the cost, parts, and expectations. A full tutorial
category: article
image: /media/thumbnails/slimevr.png
---

# Goal

To build a Full body set of trackers:

* 7 trackers
* 3 extensions


# Parts List & Prices

**NOTE:** *Some extra components were purchased in case of defects*

:warning: Huge caveats about the purchase list below :warning:

1. Buy more parts than I did, ( especially BMI's ) because
some of these have a horrible failure rate. 15+

2. Maybe throw in an extra D1 ( 11 total ) because these can fail too.

3. Batter chargers tend to be fine but can be prone to getting burned so be careful when soldering.

## Main parts from AliExpress:

* `10` D1 Mini for `20.53$`
* `13` BMI 160 for `18.99$`
* `10` 18650 Battery **chargers** for `6.81$`
    * Keep in mind that this will also work for regular LIPO batteries
* *See battery section for explanation*

## Small parts from AliExpress:

* 10 pack of DPDT 2P2T Panel Mount Switches for `2.99$`
* 100 pack of 30K ohms for `4.05$`
* 5 pack of 4Pin JST connections for `4.93$`
* 50 pack of diodes for `2.53$`

## Batteries

For reference I bought:

* `1` ( 10 piece ) EHAO 804040 3.7V 18mAh for `32.77$`

While these do work, shipping time is pretty awful compared to everything else.
Looking at the order of a few months for some vendors.
To deal with this problem we can swap those out for some `18650`'s:

* 10 pack of battery clips `7.99$`
* Set of 10 batteries for `33.32$`

* Total with taxes: `41.14$`

I can attest that `18650batteries.com` is a reputable place to get batteries.
*SideNote: includes shipping but you're mileage may vary depending on source*

## Total Initial Cost With Shipping and Taxes

If you buy the 804040 batteries: `93.60$`

With the 18650's: `101.97$`

## Assembly

### Assumptions

That the parts list above is being used however the points below
are kept somewhat general.

### Equipment

Important to note that if you don't have soldering equipment you'll need
to get some.
Cheap kits can be found for under 50$ but make sure you have plenty of solder.

1. 18650's are heavy so if possible

For this reason it's not a bad idea to get some battery clips and mount 
everything to that to keep things compact. When the weight of a tracker
is imbalanced then it's much harder to mount as it tends to "swing" more.

2. Heat shrink of varying sizes to clean up splices is basically required.

3. \*Flux if you want however this is really not required at all.
