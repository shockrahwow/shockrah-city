---
title: Building a Video Streaming Service with Rust
date: 2021-13-10
draft: true
description: The real state of Rocket right now
category: article
---

# What I used

* Rocket.rs - a framework written in rust that reminds me a lot of Flask

Coming from Hyper.rs this is a massive change of pace since I mostly get to concern myself with application level logic instead of transport layer logic.
This was a common theme with Hyper and even more so if I wanted to do any kind of streaming.

* Tera Templates - because they're basically just GO templates

Also Rocket also comes with very smooth integrations for using Tera templates; so much so that there's basically no setup.

