---
title: "Qutebrowser is /comfy/"
date: 2018-07-18
draft: false
description: Goodbye Chromium
category: article
---

# A Taste of QuteBrowser

I finally sat down and started fully using [Qutebrowser](https://www.qutebrowser.org/) and to be honest, it's great. 
I had tried using it before but I never bothered to learn how to to use it nor did I try to rice it to be liking. 
However there are two things which came be a pleasant surprise about the browser:

1. No ricing past basic setup involved for comfortable use
2. There were way more /comfy/ reasons to like this over other browsers

I'm was Chromium user for a while since the adblocking and extension support was ultra convinient for me to take advantage of, and it let's me sync things together across devices, _somthing about being a good goy here_.
Say what you want about syncing across devices and """_the cloud_""" but its pretty comfy since everything is setup essentially where-ever you go.
In other words Chrome and the like get the good ole' stamp of _werks for me_.

{{< pic "/media/img/werks.png" >}}

## Out of the box experience

Personally the only thing I did was change the default starting page for new windows and new tabs.
Which of course is my own personal /comfy/ [start-page](https://gitlab.com/AlejandroS/start-page-tab).
After that, there was basically no more setup since most/all of the keybindings are sensible enough to warrant not changing them, _imo_.
The only functionality I couldn't figure out at first was escaping out of drop-down menus and/or text-boxes.
I did however come up with this:

```
"e":"fake-key <escape>"
```
Just enter the command `:set` to get to your config page, make the changes you want, like adding the above in your aliases section, then run the command `config-write-py`. 
If you have to save over an old config file just use `config-write-py --force`.
However, even without this the browser is still massively usable and convenient, it's just that you're able to rice the _fug_ out of this but I personally didn't that route.

Mouse control is just like any other browser I've ever used so there's that too.
Text boxes are usually auto-detected by the browser so that you are put into `insert mode` just like in vim. 

Some nice things that I use constantly are the commands below, which don't require command mode:
* yy - copies page url to clipboard
* wp - opens new window with clipboard contents as url-argument
* r - sick refresh
* D - deletes tab

## Main Gripes

Keep in mind that all of these complaints, _except the last one_ can be mitigated by changing a setting once in your config and forgetting about it forever.
Also if you use qutebrowser on multiple machines consider copying that config file over to usb and using that to set yourself up wherever you are.
> 'J' move right and 'K' move right along the tab list

If you look at it as a horizontal list then yes it doesn't make sense. Instead think of the tabs as vertically aligned and the rest should follow suit.

```
---------
first
---------
second
---------
third
---------
```
Rebinding or just getting used to it are the "_fixes_" for this since it is such a minor coplaint about the out-of-the-box experience.

> ***Adveritements Everywhere***

The built-in adblocker is just a host list which means it won't exactly do ~~much~~ anything on most websites.
Plugin support is coming soon but for now adblocking is basically non-existant if you browser _any_ modern websites.
Hopefully we get a proper ad-blocker in the future that can keep things a bit cleaner like _Adblocker+_ or better yet _Ublock Origin_.

## Bindings and Defualts mostly

There are plenty of binds but if you just read the docs then you should be fine. 

[Link to official binding cheat sheet](https://raw.githubusercontent.com/qutebrowser/qutebrowser/master/doc/media/img/cheatsheet-big.png)

Just like any program with tons of keyboard shortcuts, it's better to learn whatever you need and progressively learn more commands as you need them.

## Is it worth it tho?

Ya. It's a good browser, it's solid and besides a few minor hiccups it's very close to BTFO every other browser out there.
If you don't like using keyboard shortcuts, and prefer to use the mouse however, stick to using what you are most comfortable with.
In general if you are comfortable with shortcuts and want to support a free privacy-respecting(_as far as I know_) browser; also you can rice it :^).
