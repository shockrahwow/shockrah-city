from os import environ
from requests import get
from json import dumps

def request(key: str, uri: str):
    url = f'https://neocities.org{uri}'
    headers = { 'Authorization': f'Bearer {key}' }
    response = get(url, headers=headers)
    return response.json()

def get_new_content(key: str):
    '''
    Fetches a list of all files on neocities 
    '''
    # First fetch the hashes of all our files
    response = request(key, '/api/list')
    remote_files = response['files']

    # COmpare remote hashes to local hashes


if __name__ == '__main__':
    key = environ.get('NEOCITIES_API_KEY')
    if key is None:
        print('Check to ensure NEOCITIES_API_KEY is set in the env vars')
        exit(1)
    else:
        get_public_contents(key)

