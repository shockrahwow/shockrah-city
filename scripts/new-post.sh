#!/bin/bash -e

[[ "$1" = "" ]] && echo No filename given for new post\! && exit 1

name="$(basename -s .md $1)"

echo Creating new post content/posts/$name.md with metadata:
cat << EOF | tee content/posts/"$name.md"
---
title: $(basename -s .md "$file")
date: $(date '+%F')
draft: false
description:
category: article
image:
---
EOF
