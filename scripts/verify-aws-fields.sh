#!/bin/bash

set -e

echo "Checking to make sure all required AWS env vars are present"

[[ $AWS_DEFAULT_REGION != '' ]]
echo AWS_DEFAULT_REGION  is set

[[ $AWS_DEFAULT_REGION != '' ]]
echo AWS_ACCESS_KEY_ID  is set

[[ $AWS_DEFAULT_REGION != '' ]]
echo AWS_SECRET_ACCESS_KEY  is set

